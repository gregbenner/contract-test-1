'use strict';

(function() {
  describe('favouriteLanguageService Spec', function() {

    var favouriteLanguageService;

    beforeEach(function() {
      angular.module('unboxedConsulting');
    });

    beforeEach(inject(function() {
      var $injector = angular.injector(['unboxedConsulting']);
      favouriteLanguageService = $injector.get('favouriteLanguage');
    }));

    it('should define getFavouriteLanguage', function(){
      expect(favouriteLanguageService.getFavouriteLanguage).toBeDefined();
    });

    it('should define filterRepos', function(){
      expect(favouriteLanguageService.filterRepos).toBeDefined();
    });

  });
}());
