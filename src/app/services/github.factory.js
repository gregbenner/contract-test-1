'use strict';

angular.module('unboxedConsulting')
.factory('githubResource', function githubResource($http) {
  var githubResource = {};

  githubResource.getRepos = function(username) {
      return $http.get('https://api.github.com/users/' + username + '/repos');
  };

  return githubResource;
});
