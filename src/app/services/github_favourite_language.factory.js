'use strict';

angular.module('unboxedConsulting')
.factory('favouriteLanguage', function favouriteLanguage($resource) {
  var favouriteLanguage = {},
      favouriteLanguageIs = '',
      amountOfRepos = null,
      differentLanguages = [];

  function mode(arr){
    return arr.sort(function(a,b){
        return arr.filter(function(v){ return v===a }).length
             - arr.filter(function(v){ return v===b }).length
    }).pop();
  }

  favouriteLanguage.getFavouriteLanguage = function(data) {
    // filter the data
    this.filterRepos(data);
    // utility to get greatest occurence of a value in an array
    favouriteLanguageIs = mode(differentLanguages);

    return favouriteLanguageIs;
  };

  favouriteLanguage.filterRepos = function(data) {
    // how many total repos
    amountOfRepos = data.length;

    // loop the results
    angular.forEach(data, function(value, key) {
      // push languages into an array
      differentLanguages.push(value.language);
    });

    return differentLanguages;
  };

  return favouriteLanguage;
})
