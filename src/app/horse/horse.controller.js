'use strict';

angular.module('unboxedConsulting')
  .controller('HorseCtrl', function ($scope) {
    $scope.component = {};
    $scope.component.imgSrc = '/assets/images/horse-js_normal.png';
    $scope.component.time = "7 days";
    $scope.component.title = "horse_js";
    $scope.component.text = "as much as I want to be, I'm not";
    $scope.component.username = "horse_js";
  });
