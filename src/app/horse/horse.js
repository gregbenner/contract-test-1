'use strict';

angular.module('unboxedConsulting')
.config(function ($stateProvider) {
    $stateProvider
      .state('horse', {
        url: '/horse',
        templateUrl: 'app/horse/horse.html',
        controller: 'HorseCtrl'
      });
    });
