'use strict';

angular.module('unboxedConsulting')
  .controller('MainCtrl', function ($scope, githubResource, favouriteLanguage) {
    var favouriteLanguageCheck;
    $scope.favouriteLanguage = '';

    $scope.searchusername = function(username) {
     githubResource.getRepos(username)
      .success(function(results) {
        favouriteLanguageCheck(results);
      });
    };

    favouriteLanguageCheck = function(data) {
      $scope.favouriteLanguage = favouriteLanguage.getFavouriteLanguage(data);
    };

  });
