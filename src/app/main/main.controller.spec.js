'use strict';

describe('controllers', function(){
  var scope;

  beforeEach(module('unboxedConsulting'));

  beforeEach(inject(function($rootScope) {
  	scope = $rootScope.$new();
  }));

  it('should define favouriteLanguage', inject(function($controller) {

    $controller('MainCtrl', {
      $scope: scope
  	});

    expect(scope.favouriteLanguage).toBeDefined();
  }));
});
