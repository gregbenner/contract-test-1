'use strict';

angular.module('unboxedConsulting', ['ngResource', 'ui.router'])
  .config(function ($urlRouterProvider) {
    $urlRouterProvider.otherwise('/');
  })
;
